package screens;

import base.screenBaseClass;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class settingsScreen extends screenBaseClass{

    //ELEMENTS

    @AndroidFindBy(id = "com.verb.messenger:id/profileAddAvatarImage")
    public MobileElement uploadAvatar;

    @AndroidFindBy(id = "com.verb.messenger:id/profileSettingsOptions")
    public MobileElement settingsOptions;



    @AndroidFindBy(id = "com.verb.messenger:id/profileNotificationsSettings")
    public MobileElement notificationSettings;

    @AndroidFindBy(id = "com.verb.messenger:id/profileAddAvatarImage")
    public MobileElement profileImage;

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Take a photo')]")
    public MobileElement takeAPhoto;

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Select from gallery')]")
    public MobileElement selectGalery;

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Select from memory')]")
    public MobileElement selectMemory;

    @AndroidFindBy(id = "com.android.permissioncontroller:id/permission_allow_button")
    public MobileElement permBtn;

    @AndroidFindBy(id = "com.android.camera:id/v9_shutter_button_internal")
    public MobileElement takePicture;

    @AndroidFindBy(id = "com.android.camera:id/intent_done_apply")
    public MobileElement takePictureCheckBtn;

    @AndroidFindBy(id = "com.verb.messenger:id/cropImageDoneButton")
    public MobileElement takePictureDoneBtn;


    //INIT ELEMENTS

    public settingsScreen(AndroidDriver<MobileElement> driver) {
        super(driver);
        //initialize elements above
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    //METHODS

    public void allowImageAccess(){
        if (permBtn.isDisplayed()){
            permBtn.click();
        }else {
            System.out.println("Permissions already accepted");
        }
    }

    public void selectAvatarOption(MobileElement element){
        element.click();
    }

    public void takePhoto(){
        waitForElementToBePresent(driver, 10, takePicture).click();
        takePictureCheckBtn.click();
        takePictureDoneBtn.click();
        waitForElementToBePresent(driver, 10, profileImage);
    }

    public void uploadProfilePhoto(){
        profileImage.click();
    }
}
