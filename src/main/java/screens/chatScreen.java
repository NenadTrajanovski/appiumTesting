package screens;

import base.screenBaseClass;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

import org.openqa.selenium.support.PageFactory;


public class chatScreen extends screenBaseClass{



    //ELEMENTS

    @AndroidFindBy(id = "android:id/autofill_dataset_picker")
    public MobileElement autoFill;

    @AndroidFindBy(xpath = "//*[contains(@text, 'Chats')]")
    public MobileElement chats;

    @AndroidFindBy(id = "com.verb.messenger:id/calls_page")
    public MobileElement phoneCall;

    @AndroidFindBy(id = "com.verb.messenger:id/edit_profile")
    public MobileElement editProfile;

    @AndroidFindBy(id = "com.verb.messenger:id/new_chat")
    public MobileElement newChat;

    @AndroidFindBy(id = "com.verb.messenger:id/profileSettingsOptions")
    public MobileElement profileSettings;

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Edit name')]")
    public MobileElement editName;

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Log out')]")
    public MobileElement logOut;

    @AndroidFindBy(id = "com.verb.messenger:id/profileFirstNameTextField")
    public MobileElement firstName;

    @AndroidFindBy(id = "com.verb.messenger:id/profileLastNameTextField")
    public MobileElement lastName;

    @AndroidFindBy(id = "com.verb.messenger:id/user_editing_done_menu_item")
    public MobileElement doneEditName;

    @AndroidFindBy(xpath = "android.widget.ImageButton")
    public MobileElement backFromSettings;

    @AndroidFindBy(id = "com.verb.messenger:id/profileFullNameLabel")
    public MobileElement fullName;

    //INIT ELEMENTS

    public chatScreen(AndroidDriver<MobileElement> driver) {
        super(driver);
        //initialize elements above
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    //METHODS

    //HIDE AUTOFILL
/*    public void hideAutoFill(){
        hideAutoFill(autoFill);
    }*/

    public void setFirstLastName(String firstName, String lastName){
        waitForElementToBePresent(driver, 10, this.firstName).clear();
        if (autoFill.isDisplayed()){
            driver.pressKey(new KeyEvent(AndroidKey.BACK));
        }else {
            System.out.println("no autopicker is displayed");
        }
        this.firstName.sendKeys(firstName);

        waitForElementToBePresent(driver, 10, this.lastName).clear();
        this.lastName.sendKeys(lastName);
    }

    public void setAutomationUserName(){
        profileSettings.click();
        editName.click();

        setFirstLastName("Automation", "User");
        doneEditName.click();
        profileSettings.click();
        logOut.click();
    }


}
