package screens;

import base.screenBaseClass;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Wait;

import java.net.MalformedURLException;

public class messagingScreen extends screenBaseClass {

    //ELEMENTS

    @AndroidFindBy(id = "com.android.mms:id/subject")
    public MobileElement sms;

    //INIT ELEMENTS

    public messagingScreen(AndroidDriver<MobileElement> driver) {
        super(driver);
        //initialize elements above
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    //METHODS

    public void switchToSms(){
        driver.startActivity(new Activity("com.android.mms", ".ui.ConversationList"));
    }

    public void getSmsCode(){

        sms.getText();
    }


}
