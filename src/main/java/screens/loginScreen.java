package screens;

import base.screenBaseClass;

import com.google.common.collect.Iterables;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;



public class loginScreen extends screenBaseClass{


    //ELEMENTS
    @AndroidFindBy(id = "android:id/autofill_dataset_picker")
    public MobileElement autoFill;

    @AndroidFindBy(id = "com.verb.messenger:id/textView")
    public MobileElement textMessage;

    @AndroidFindBy(id = "com.verb.messenger:id/auth_country_name")
    public MobileElement countrySelector;

    @AndroidFindBy(id = "android:id/search_src_text")
    public MobileElement searchCountry;

    @AndroidFindBy(id = "android:id/search_close_btn")
    public MobileElement cancelSearch;

    @AndroidFindBy(id = "com.verb.messenger:id/country_code_row_country_name")
    public MobileElement countries;

    @AndroidFindBy(id = "com.verb.messenger:id/auth_phone_number_field")
    public MobileElement phoneNum;

    @AndroidFindBy(id = "android:id/message_text")
    public MobileElement validationCode;

    @AndroidFindBy(id = "com.verb.messenger:id/auth_submit_button")
    public MobileElement doneBtn;

    @AndroidFindBy(id = "android:id/button1")
    public MobileElement yesBtn;

    @AndroidFindBy(id = "android:id/button2")
    public MobileElement noBtn;

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Verb')]")
    public MobileElement verbApp;

    @AndroidFindBy(id = "com.android.permissioncontroller:id/permission_allow_button")
    public MobileElement permBtn;

    @AndroidFindBy(id = "com.verb.messenger:id/toolbarTitle")
    public MobileElement phoneBar;

    @AndroidFindBy(id = "com.verb.messenger:id/button3")
    public MobileElement sendCodeAgain;

    @AndroidFindBy(xpath = "//android.widget.Button[contains(@text,'Mark as read')]")
    public MobileElement markAsRead;

    @AndroidFindBy(id = "android:id/message")
    public MobileElement incorrectCodeMsg;

    @AndroidFindBy(id = "android:id/button1")
    public MobileElement okBtnIncorrect;


    //INIT ELEMENTS

    public loginScreen(AndroidDriver<MobileElement> driver) {
        super(driver);
        //initialize elements above
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    //METHODS

    //HIDE AUTOFILL
    public void hideAutoFill(){
        hideAutoFill(autoFill);
    }

    //Generic click method
    public void genericClick(MobileElement element){
        element.click();
    }

    //Generic set Text method
    public void genericSetText(MobileElement element, String text){
        element.sendKeys(text);
    }

    //Generic get text method
    public void getTextFromElement(MobileElement element){
        element.getText();
        System.out.println(element.getText());
    }

    //Setting country
    public void setCountry(String country){
        searchCountry.click();
        searchCountry.sendKeys(country);
        countries.click();
    }

    public void switchToVerb(){
        driver.pressKey(new KeyEvent(AndroidKey.APP_SWITCH));
        verbApp.click();
    }

    public void openNotifications(){

        driver.openNotifications();
        waitForElementToBePresent(driver, 20, validationCode);
    }


    //Setting validation code
    public void setCode(){

        openNotifications();
        String opt = validationCode.getText().split(" -")[0].replace(" ", "");
        String[] validationCode = opt.split("");

        markAsRead.click();
        switchToVerb();

        Actions ac = new Actions(driver);

        for (int i = 0; i < validationCode.length; i++){
            try {
                Thread.sleep(500);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
            ac.sendKeys(validationCode).build().perform();
        }

        try {
            Thread.sleep(1000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    public void secondCode(){
        sendCodeAgain.click();
        try {
            Thread.sleep(3000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    public void setResendCode(){
        openNotifications();

        markAsRead.click();
        switchToVerb();

        secondCode();

        openNotifications();

        String opt = validationCode.getText().split(" -")[0].replace(" ", "");
        String[] validationCode = opt.split("");

        markAsRead.click();
        switchToVerb();

        Actions ac = new Actions(driver);

        for (int i = 0; i < validationCode.length; i++){
            try {
                Thread.sleep(500);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
            ac.sendKeys(validationCode).build().perform();
        }
    }

    public void setIncorrectCode(){
        String incorrectCode = "000000";
        Actions ac = new Actions(driver);

        for (int i = 0; i < incorrectCode.length(); i++){
            try {
                Thread.sleep(500);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
            ac.sendKeys(incorrectCode).build().perform();
        }

    }

    //Checking and setting permissions
    public void setPermissions(){
        if (permBtn.isDisplayed()){
            permBtn.click();
        }if (permBtn.isDisplayed()){
            permBtn.click();
        }else {
            System.out.println("Permissions already accepted");
        }
        try {
            Thread.sleep(6000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    //Two minute wait
    public void twoMinuteWait(){
        //We have to brake the > 2 minute wait
        // as Appium closes session after 60 seconds of inactivity
        try {
            Thread.sleep(50000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }

        Assert.assertEquals(phoneBar.getText(), "+389 77 637 699");
        phoneBar.click();

        try {
            Thread.sleep(50000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }

        Assert.assertEquals(phoneBar.getText(), "+389 77 637 699");
        phoneBar.click();

        try {
            Thread.sleep(30000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    //Locks and unlocks the phone
    public void lockUnlockDevice(){
        driver.lockDevice();
        try {
            Thread.sleep(1000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        driver.unlockDevice();
    }

    public void waitFewSeconds(){
        try {
            Thread.sleep(4000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }




}
