package base;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class screenBaseClass {

    public static AndroidDriver<MobileElement> driver;
    public WebDriverWait wait;


    public screenBaseClass(AndroidDriver<MobileElement> driver){

        this.driver = driver;
    }

    public void genericClick(MobileElement element){
        element.click();
    }

    public void hideKeyboard(){
        driver.hideKeyboard();
    }

    public void hideAutoFill(MobileElement element){
        if (element.isDisplayed()){
            driver.pressKey(new KeyEvent(AndroidKey.BACK));
        }else {
            System.out.println("no autopicker is displayed");
        }
    }

    public void enter(){

        ((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.ENTER));
    }

    public MobileElement waitForElementToBeClickable(AndroidDriver driver, int timeOut, MobileElement element){
        WebDriverWait wait = new WebDriverWait(driver, timeOut);
        return (MobileElement) wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public MobileElement waitForElementToBePresent(AndroidDriver driver, int timeOut, MobileElement element){
        WebDriverWait wait = new WebDriverWait(driver, timeOut);
        return (MobileElement) wait.until(ExpectedConditions.visibilityOf(element));
    }
}
