package extentListeners;

import base.screenBaseClass;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class extentManager {

    private static ExtentReports extent;

    public static ExtentReports createInstance(String fileName){

        ExtentSparkReporter htmlReporter = new ExtentSparkReporter(fileName);

        htmlReporter.config().setTheme(Theme.DARK);
        htmlReporter.config().setDocumentTitle(fileName);
        htmlReporter.config().setEncoding("utf-8");
        htmlReporter.config().setReportName(fileName);

        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);
        extent.setSystemInfo("Automation Tester", "Nenad Trajanovski");
        extent.setSystemInfo("Application", "Verb");

        return extent;

    }

    public static String screenShotPath;
    public static String screenShotName;

    public static void captureScreenshot(){

        File scrFile = ((TakesScreenshot) screenBaseClass.driver).getScreenshotAs(OutputType.FILE);

        Date d = new Date();
        screenShotName = d.toString().replace(":", "_").replace(" ", "_") + ".jpg";

        try {
            FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir") + "\\reports\\" + screenShotName));
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
