package base;


import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import org.apache.commons.io.FileUtils;



import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import org.testng.Assert;
import utilities.appiumServer;
import utilities.commonUtils;
import utilities.scrollUtil;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

public class testBase {


    public AndroidDriver<MobileElement> driver;
    public static String loadPropertyFile = "Android_flipboard.properties";
    public static Logger log = Logger.getLogger(String.valueOf(testBase.class));


    public void takeScreenShot() {

        Date d = new Date();
        String fileName = d.toString().replace(":", "_").replace(" ", "_") + ".jpg";
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

        try {
            FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir") + "\\screenshots\\" + fileName));
        }catch (IOException e){
            e.printStackTrace();
        }

        Tesseract instance = new Tesseract();


        try {
            String result = instance.doOCR(scrFile);
            System.out.println(result);
            Assert.assertTrue(result.contains("Valid phone number required"));
        } catch (TesseractException e) {
            e.printStackTrace();
        }

    }

    public void setUp(){
            //PropertyConfigurator.configure(System.getProperty("user.dir") + "\\src\\test\\resources\\properties\\log4j.properties");

                appiumServer.start();
                log.info("Appium server started");

                commonUtils.loadAndroidConfigProperty(loadPropertyFile);
                log.info(loadPropertyFile + " properties file loaded");

                commonUtils.setAndroidCapabilities();
                driver = (AndroidDriver<MobileElement>) commonUtils.getAndroidDriver();

        }

        public void swipeUp(){
            scrollUtil.scrollUp(1, driver);
        }


    public void tearDown(){

        driver.quit();
        log.info("Test case execution completed");

        appiumServer.stop();
        log.info("Appium server stopped");
    }

}
