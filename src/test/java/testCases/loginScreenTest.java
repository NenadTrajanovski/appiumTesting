package testCases;

import base.testBase;
import org.testng.Assert;
import org.testng.annotations.*;

import screens.loginScreen;
import screens.chatScreen;
import base.screenBaseClass;
import java.util.concurrent.TimeUnit;


public class loginScreenTest extends testBase {

    loginScreen ls;
    chatScreen cs;
    screenBaseClass sbc;



    @BeforeMethod
    public void init(){
        setUp();
        ls = new loginScreen(driver);
        cs = new chatScreen(driver);
        sbc = new screenBaseClass(driver);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }



    @Test(testName = "Login basic flow", priority = 1)
    public void loginBasic(){
        ls.hideAutoFill();
        Assert.assertEquals(ls.textMessage.getText(), "Please confirm your country code and enter your phone number");
        ls.genericClick(ls.countrySelector);
        ls.setCountry("Macedonia");
        ls.hideAutoFill();
        ls.genericSetText(ls.phoneNum, "77637699");
        ls.genericClick(ls.doneBtn);

        ls.genericClick(ls.yesBtn);
        ls.setCode();

        ls.setPermissions();
        Assert.assertEquals(cs.chats.getText(), "Chats");
    }

    @Test(testName = "Valid phone number required", priority = 2)
    public void wrongPhoneNumber(){
        ls.hideAutoFill();
        Assert.assertEquals(ls.textMessage.getText(), "Please confirm your country code and enter your phone number");
        ls.genericSetText(ls.phoneNum, "0000");
        ls.genericClick(ls.doneBtn);
        sbc.hideKeyboard();
        takeScreenShot();
    }

    @Test(testName = "Validate after 2 minutes", priority = 3)
    public void validateWithWait(){
        ls.hideAutoFill();
        Assert.assertEquals(ls.textMessage.getText(), "Please confirm your country code and enter your phone number");
        ls.genericClick(ls.countrySelector);
        ls.setCountry("Macedonia");
        ls.hideAutoFill();
        ls.genericSetText(ls.phoneNum, "77637699");
        ls.genericClick(ls.doneBtn);

        ls.genericClick(ls.yesBtn);

        ls.twoMinuteWait();
        ls.setCode();

        ls.setPermissions();
        Assert.assertEquals(cs.chats.getText(), "Chats");
    }

    @Test(testName = "Resend code function", priority = 4)
    public void resendCode(){
        ls.hideAutoFill();
        Assert.assertEquals(ls.textMessage.getText(), "Please confirm your country code and enter your phone number");
        ls.genericClick(ls.countrySelector);
        ls.setCountry("Macedonia");
        ls.hideAutoFill();
        ls.genericSetText(ls.phoneNum, "77637699");
        ls.genericClick(ls.doneBtn);

        ls.genericClick(ls.yesBtn);
        ls.waitFewSeconds();
        ls.setResendCode();

        ls.setPermissions();
        Assert.assertEquals(cs.chats.getText(), "Chats");
    }

    @Test(testName = "Validate after phone goes to sleep", priority = 5)
    public void validateAfterSleep(){
        ls.hideAutoFill();
        Assert.assertEquals(ls.textMessage.getText(), "Please confirm your country code and enter your phone number");
        ls.genericClick(ls.countrySelector);
        ls.setCountry("Macedonia");
        ls.hideAutoFill();
        ls.genericSetText(ls.phoneNum, "77637699");
        ls.genericClick(ls.doneBtn);

        ls.genericClick(ls.yesBtn);
        ls.lockUnlockDevice();
        swipeUp();
        ls.setCode();

        ls.setPermissions();
        Assert.assertEquals(cs.chats.getText(), "Chats");
    }

    @Test(testName = "Set incorect validation code", priority = 6)
    public void incorectCode(){
        ls.hideAutoFill();
        Assert.assertEquals(ls.textMessage.getText(), "Please confirm your country code and enter your phone number");
        ls.genericClick(ls.countrySelector);
        ls.setCountry("Macedonia");
        ls.hideAutoFill();
        ls.genericSetText(ls.phoneNum, "77637699");
        ls.genericClick(ls.doneBtn);

        ls.genericClick(ls.yesBtn);
        ls.setIncorrectCode();

        Assert.assertEquals(ls.incorrectCodeMsg.getText(), "Please try again or request new code");
        ls.okBtnIncorrect.click();

    }




    @AfterMethod
    public void quitDriver(){
        tearDown();
    }

}
