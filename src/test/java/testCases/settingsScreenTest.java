package testCases;

import base.testBase;
import org.testng.Assert;
import org.testng.annotations.*;

import screens.loginScreen;
import screens.chatScreen;
import screens.settingsScreen;
import base.screenBaseClass;
import java.util.concurrent.TimeUnit;

public class settingsScreenTest extends testBase{


    loginScreen ls;
    chatScreen cs;
    screenBaseClass sbc;
    settingsScreen ss;


    @BeforeMethod
    public void init(){
        setUp();
        ls = new loginScreen(driver);
        cs = new chatScreen(driver);
        sbc = new screenBaseClass(driver);
        ss = new settingsScreen(driver);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test(testName = "Logout flow", priority = 1)
    public void logOutFlow(){
        ls.hideAutoFill();

        sbc.genericClick(ls.countrySelector);
        ls.setCountry("Macedonia");
        ls.hideAutoFill();
        ls.genericSetText(ls.phoneNum, "77637699");
        sbc.genericClick(ls.doneBtn);

        sbc.genericClick(ls.yesBtn);
        ls.setCode();

        ls.setPermissions();
        Assert.assertEquals(cs.chats.getText(), "Chats");

        sbc.genericClick(cs.editProfile);
        sbc.genericClick(cs.profileSettings);
        sbc.genericClick(cs.logOut);
    }

    @Test(testName = "Enter numbers and special characters in name", priority = 2)
    public void setNumsAndSpecChars(){
        ls.hideAutoFill();

        sbc.genericClick(ls.countrySelector);
        ls.setCountry("Macedonia");
        ls.hideAutoFill();
        ls.genericSetText(ls.phoneNum, "77637699");
        sbc.genericClick(ls.doneBtn);

        sbc.genericClick(ls.yesBtn);
        ls.setCode();

        ls.setPermissions();
        Assert.assertEquals(cs.chats.getText(), "Chats");

        sbc.genericClick(cs.editProfile);
        sbc.genericClick(cs.profileSettings);
        sbc.genericClick(cs.editName);

        cs.setFirstLastName("##123$%^", "##123$%^");
        sbc.genericClick(cs.doneEditName);

        Assert.assertEquals(cs.fullName.getText(), "##123$%^ ##123$%^");

        cs.setAutomationUserName();
    }


    @Test(testName = "Validate full name after re-logging", priority = 3)
    public void editName(){
        ls.hideAutoFill();

        sbc.genericClick(ls.countrySelector);
        ls.setCountry("Macedonia");
        ls.hideAutoFill();
        ls.genericSetText(ls.phoneNum, "77637699");
        sbc.genericClick(ls.doneBtn);

        sbc.genericClick(ls.yesBtn);
        ls.setCode();

        ls.setPermissions();
        Assert.assertEquals(cs.chats.getText(), "Chats");

        sbc.genericClick(cs.editProfile);
        sbc.genericClick(cs.profileSettings);
        sbc.genericClick(cs.editName);

        cs.setFirstLastName("Test QA", "Engineer");
        sbc.genericClick(cs.doneEditName);

        sbc.genericClick(cs.profileSettings);
        sbc.genericClick(cs.logOut);

        ls.hideAutoFill();

        sbc.genericClick(ls.countrySelector);
        ls.setCountry("Macedonia");
        ls.hideAutoFill();
        ls.genericSetText(ls.phoneNum, "77637699");
        sbc.genericClick(ls.doneBtn);

        sbc.genericClick(ls.yesBtn);
        ls.setCode();

        sbc.genericClick(cs.editProfile);

        Assert.assertEquals(cs.fullName.getText(), "Test QA Engineer");

        cs.setAutomationUserName();
    }

    @Test(testName = "change Avatar with camera photo", priority = 4)
    public void avatarTakePhoto(){
        ls.hideAutoFill();

        sbc.genericClick(ls.countrySelector);
        ls.setCountry("Macedonia");
        ls.hideAutoFill();
        ls.genericSetText(ls.phoneNum, "77637699");
        sbc.genericClick(ls.doneBtn);

        sbc.genericClick(ls.yesBtn);
        ls.setCode();

        ls.setPermissions();
        Assert.assertEquals(cs.chats.getText(), "Chats");

        sbc.genericClick(cs.editProfile);
        sbc.genericClick(ss.profileImage);
        ss.selectAvatarOption(ss.takeAPhoto);
        ss.allowImageAccess();
        ss.takePhoto();

    }


    @AfterMethod
    public void quitDriver(){
        tearDown();
    }

}
